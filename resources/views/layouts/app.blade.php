<style>
.imagen1{
    width: 250px;
    height: px;
}


    
</style>



<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/estilos.css')}}" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
            <a class="navbar-brand" href="#"> <img src="/images/seq2.png" class="imagen1"  alt="logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Iniciar Sesión</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Registro</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       Cerrar Sesión
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <div class="espaciado"></div>
    <footer>
       
        <div class="container-footer-all">
         
             <div class="container-body">
 
                 <div class="colum1">
                     <h1>TRÁMITE NCORPORACIÓN DE ESCUELAS PARTICULARES</h1>
 
                     <p>OTORGAR RECONOCIMIENTOS DE VALIDEZ OFICIAL DE ESTUDIOS A ESCUELAS PARTICULARES PARA IMPARTIR EDUCACIÓN MEDIA SUPERIOR, SUPERIOR Y NORMALES.</p>
 
                 </div>
 
                 <div class="colum2">
 
                     <h1>Redes Sociales</h1>
 
                     <div class="row">
                    <a href="https://www.facebook.com/educacionqr/"><img src="icon/facebook.png"></a> 
                    <a href="https://www.facebook.com/educacionqr/"><label>Siguenos en Facebook</label></a>
                     </div>
                     <div class="row">
                        <a href="https://twitter.com/educacionqr/"><img src="icon/twitter.png"></a> 
                      <a href="https://twitter.com/educacionqr/"><label>Siguenos en Twitter</label></a> 
                     </div>
                 </div>
 
                 <div class="colum3">
 
                     <h1>Informacion Contactos</h1>
 
                     <div class="row2">
                         <img src="icon/house.png">
                         <label>Av. Insurgentes No. 600 - Col. Gonzalo Guerrero Teléfono : 01 (983) 83 - 507 - 70 C.P. 77020 - Chetumal, Quintana Roo</label>
                     </div>

                     <div class="row2">
                        <img src="icon/smartphone.png">
                        <label>(983) 83 5 07 70 </label>
                    </div>
                 </div>
 
             </div>
         
         </div>
         
         <div class="container-footer">
                <div class="footer">
                     <div class="copyright">
                         © 2020 Todos los Derechos Reservados | <a href="https://qroo.gob.mx/seq">SEQ</a>
                     </div>
 
                     <div class="information">
                         <a href="">Informacion Compañia</a> | <a href="">Privacion y Politica</a> | <a href="">Terminos y Condiciones</a>
                     </div>
                 </div>
 
             </div>
         @yield('footer')
     </footer>

</body>


</html>
