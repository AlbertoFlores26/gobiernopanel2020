@extends('layouts.app')


@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">informacion</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form>
                        <div class="form-row">
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault01">Nombre</label>
                          <input type="text" class="form-control" id="validationDefault01" value="{{auth()->user()->name}}" required>
                          </div>
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault02">Correo electronico</label>
                            <input type="email" class="form-control" id="validationDefault02" value="{{auth()->user()->email}}" required>
                          </div>
                        </div>
                      
                        <div class="form-group">
                          <div class="form-check">
                            
                          </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Submit form</button>
                      </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection



